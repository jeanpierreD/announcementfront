import { createApp } from 'vue';
import App from './App.vue';
import List from './components/Announcement/List.vue';
import Show from './components/Announcement/Show.vue';
import AnnouncementPartialCard from './components/Announcement/AnnouncementPartialCard.vue';
import ShowLineAnnouncement from './components/Custom/ShowLineAnnouncement.vue';
import { createWebHistory, createRouter } from "vue-router";

const routes = [
    { path: '/', component: List },
    { path: '/announcement/:id', component: Show}
];

const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHistory(),
    routes, // short for `routes: routes`
  })

const app = createApp(App);

app.component('AnnouncementPartialCard', AnnouncementPartialCard);
app.component('ShowLineAnnouncement', ShowLineAnnouncement);
app.use(router);
app.mount('#app');
